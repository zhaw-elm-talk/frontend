# Elm

Joris Morger - Nicolas Gagliani

---

### Motivation

- Writing GUI's in functional languages is easy! (and fun)
- F# (e.g. Xamarin Forms)
- PureScript, Elm -> Go native with Electron
- Tooling keeps getting better (Atom, VS Code)

---

![purescriptUI](images/purescriptUI.png)

---

### Values

```elm
number : Int
number = 5

greetings : String
greetings = "Hello World"
```

---

### Functions

```elm
add : Int -> Int -> Int
add a b = a + b

add42 : Int -> Int
add42 n = add 42 n
-- add42 8 == 50

add43 : Int -> Int
add43 = add 43
-- add43 7 == 50
```

---

### Tuples

```elm
person : (String, String, Int)
person = ("Hans Meier", "Zürich", 8000)
```

---

### Pattern Matching

```elm
fizzBuzz n =
    case (n % 3, n % 5) of
        (0, 0) -> "Fizz Buzz"
        (0, _) -> "Fizz"
        (_, 0) -> "Buzz"
        (_, _) -> toString n
```

---

### Lists

```elm
list : List Int
list = [1, 2, 3, 4]

prepend : List a -> a -> List a
prepend xs x = x :: xs
-- prepend list 0 == [0, 1, 2, 3, 4]

append : List a -> a -> List a
append xs x = xs ++ [x]
-- append list 5 == [1, 2, 3, 4, 5]

-- List.sort [2, 4, 1, 3] == [1, 2, 3, 4]
```

---

### Tagged Unions

```elm
type Answer = Yes | No

type Name = HasName String | NoName

-- builtin
type Maybe a = Just a | Nothing

-- builtin
type Result error value = Ok value | Error error
```

---

### Working with Tagged Unions

```elm
type Name = HasName String | NoName

printName : Name -> String
printName name =
  case name of
    HasName n -> n
    NoName -> "I don't have a name :("

-- Usage

printName (HasName "Frank") -- "Frank"

printName NoName -- "I don't have a name :("
```

---

### Records

```elm
type alias Player =
  { name : String
  , x : Float
  , y : Float
  , score : Int
  }

player1 : Player
player1 =
  { name = "Player 1"
  , x = 100
  , y = 100
  , score = 0
  }

player2 = Player "Player 2" 150 150 0
```

---

### Working with Records

```elm
point = { x = 100, y = 150 }

yCoord1 = point.y -- 150

yCoord2 = .y point -- 150

List.map .y [point, point, point] -- [150, 150, 150]
```

---

```elm
point = { x = 100, y = 150 }

updatedPoint = { point | x = 150 }
-- updatedPoint == { x = 150, y = 150 }
-- point == { x = 100, y = 150 }
```

---

### Anonymous Functions

```elm
-- named function
successor : number -> number
successor n = n + 1

List.map successor [1, 2, 3]

-- anonymous function
List.map (\n -> n + 1) [1, 2, 3]
```

---

### Maybe

```elm
type Maybe a = Just a | Nothing

justOne = Just 1
nothing = Nothing

parseNumber : String -> Maybe Int
parseNumber string = ...

-- usage
parseNumber "hello" : Maybe Int -- Nothing

parseNumber "23" : Maybe Int -- Just 23
```

---

### Result

```elm
type Result error value = Ok value | Err error
```

---

### Modules

```elm
module Game where

import List                  -- List.map, List.fold, ...
import List as L             -- L.map, L.fold, ...
import List exposing (..)    -- map, fold, ...
import List exposing (map)   -- map
```

---

## Elm Architecture

---

### Model

```elm
type alias Model =
    { counter : Int
    }

initialState : Model
initialState =
    { counter = 0
    }
```

---

### Update

```elm
type Action
    = Increase
    | Decrease

update : Action -> Model -> Model
update action model =
    case action of
        Increase ->
            { model
            | counter = model.counter + 1
            }
        Decrease ->
            { model
            | counter = model.counter - 1
            }
```

---

### View

```elm
view : Address Action -> Model -> Html
view address model =
    div []
        [ button [ onClick address Incement ] [ text "+" ]
        , div [] [ text (toString model.counter) ]
        , button [ onClick address Decrement ] [ text "-" ]
        ]
```

---

### Glue

```elm
type alias Config model action =
    { init : (model, Effects action)
    , update : action -> model -> (model, Effects action)
    , view : Address action -> model -> Html
    , inputs : List (Signal action)
    }

type aliad App =
    { html : Signal Html
    , model : Signal model
    , tasks : Signal (Task Never ())
    }

start : Config model action -> App model
```

---

```elm
app : App
app =
    start
        { init = (initialState, Effects.none)
        , update = update
        , view = view
        , inputs = []
        }

main : Signal Html
main = app.html

port tasks : Signal (Task.Task Never ())
port tasks =
    app.tasks
```


---

## Documentation

- http://elm-lang.org
- http://elm-lang.org/docs
- http://package.elm-lang.org/packages
