# Elm - F# Chat Lab


## Goal
Try to recreate this chat:
http://elm-talk-backend.herokuapp.com/

## Install Elm

### Elm 0.16

http://elm-lang.org/install

### Install Editor + Plugin

#### Visual Studio Code

- Download: https://code.visualstudio.com/
- Press `ctrl+shift+p`
- type `Extensions: Install Extension`
- type `Elm` and install

#### Atom

- Download: https://atom.io/
- Go to Settings -> Install
- Search for `language-elm`
- Install

#### Emacs, Vim, ...

You're on your own

## Get Code

### Option 1 (recommended)
Use prepared and hosted API at: `http://elm-talk-backend.herokuapp.com`.
This is already the hardcoded default in the elm app, so you don't need
to change anything with this option.

### Option 2
1. `git clone https://gitlab.com/zhaw-elm-talk/backend.git`
2. Linux/MacOSX: `./build.sh`, Windows `./build.bat`
Backend should now serve requests on `http://127.0.0.1:8083`
3. Change URL in Mail.elm to `http://127.0.0.1:8083`

## Requests
Following requests are available:

Register User:

```
curl -H "Content-Type: application/json" -X POST -d '{ "name": "hans muster" }' \
 http://elm-talk-backend.herokuapp.com/register
```

response:

```json
{
"name": "hans muster",
"uuid": "618e326d-e892-4d54-b90a-d83fd3fee14b"
}
```

List all users:
  `curl -H "Content-Type: application/json" -X GET http://elm-talk-backend.herokuapp.com/users`

  reponse:
  ```json
  [{"name":"hans muster"},{"name":"other user"}]
  ```

Websocket API is not explained, since you don't need to use it for this lab.

## Run Frontend
  git clone https://gitlab.com/zhaw-elm-talk/frontend.git
  cd frontend
  elm-reactor

Now open a browser and go to http://0.0.0.0:8000/ and navigate to src/Main.elm to see your app.
The app automatically compiles if you change anything as long as you run elm-reactor, so just
hit refresh in the browser after changing something.

## Task 1
Extend the chat with the register functionality.

For this:
a) Write a json encoder and decoder in `Protocol.elm`
b) Write update functions in `Main.elm`
c) Study the register function and try to understand how the decoder, encoder and sending the request fit together

## Task 2

2a-d) write corresponding views in `Views.elm`

## Task 3 very optional
Extend backend and frontend so that people can send direct messages... Only if you have
plenty time left
