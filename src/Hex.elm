module Hex (toHex, fromHex) where

import Native.Hex


toHex : Int -> String
toHex =
  Native.Hex.toHex


fromHex : String -> Int
fromHex =
  Native.Hex.fromHex

