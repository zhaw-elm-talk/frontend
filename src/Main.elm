module Main (..) where

import Effects
import Html
import Json.Decode
import Json.Encode
import StartApp
import Task
import WebSocket
import Protocol
import Http
import Types exposing (Action(..), Model)
import View
import Signal


model : Model
model =
  { webSocket = Nothing
  , messages = []
  , currentMessage = ""
  , currentUser = Nothing
  , currentUserRegistration = ""
  , jsonError = ""
  , registrations = []
  }


{-| Connect to our websocket
-}
connect : Effects.Effects Action
connect =
  Protocol.socket
    |> Task.toMaybe
    |> Task.map Connect
    |> Effects.task


{-| Send a message to a websocket if it exists
-}
send : Maybe WebSocket.WebSocket -> String -> Effects.Effects Action
send mws m =
  case mws of
    Nothing ->
      Effects.none

    Just ws ->
      WebSocket.send m ws
        |> Task.toMaybe
        |> Task.map (always (SentMessage m))
        |> Effects.task


{-| Get the uuid of an user if it exists
-}
getUserUUID user =
  Maybe.map (\u -> u.uuid) user
    |> Maybe.withDefault ""

{-| Register a new user with a name, and get back UUID to send messages
-}
register : String -> Effects.Effects Action
register u =
  { name = u }
    |> Protocol.registrationEncoder
    |> Json.Encode.encode 0
    |> Http.string
    |> Http.post Protocol.userDecoder Protocol.registrationUrl
    |> Task.toMaybe
    |> Task.map (Maybe.map SetUser)
    |> Task.map (Maybe.withDefault NoOp)
    |> Effects.task

-- look at the actions below and fill them in
update : Action -> Model -> ( Model, Effects.Effects Action )
update action model =
  case action of
    NoOp ->
      ( model, Effects.none )

    Connect ws ->
      ( { model | webSocket = ws }, Effects.none )

    SendMessage m ->
      let
        message =
          { message = m
          , user = getUserUUID model.currentUser
          }

        encodedMessage =
          message
            |> Protocol.messageEncoder
            |> Json.Encode.encode 0
      in
        if model.currentMessage /= "" then
          ( { model | currentMessage = "" }, send model.webSocket encodedMessage )
        else
          ( model, Effects.none )

    SentMessage m ->
      ( model, Effects.none )

    ReceiveMessages mrs ->
      ( { model | messages = mrs.messages, registrations = mrs.registrations }, Effects.none )

    UpdateMessage m ->
      ( { model | currentMessage = m }, Effects.none )

    Register u ->
      -- todo: Task 1b)
      -- hint: You want to call the `register` function here
      ( model, Effects.none )

    UpdateUser u ->
      ( { model | currentUserRegistration = u }, Effects.none )

    SetUser u ->
      -- todo: Task 1b)
      -- hint: You want to update the state here with the new user
      ( model, Effects.none )

    JsonError err ->
      ( { model | jsonError = err }, Effects.none )


app : StartApp.App Model
app =
  StartApp.start
    { init = ( model, connect )
    , view = View.view
    , update = update
    , inputs = [ Signal.map decodeMessagesAndRegistrations messageFromServer.signal ]
    }


decodeMessagesAndRegistrations : String -> Action
decodeMessagesAndRegistrations s =
  case Json.Decode.decodeString Protocol.messagesAndRegistrationDecoder s of
    Ok ms ->
      ReceiveMessages ms

    Err e ->
      JsonError e


messageFromServer : Signal.Mailbox String
messageFromServer =
  Signal.mailbox ""


port sending : Task.Task String ()
port sending =
  -- will happen immediately!
  Protocol.socket
    `Task.andThen` sendAndGetMessage


sendAndGetMessage : WebSocket.WebSocket -> Task.Task String ()
sendAndGetMessage socket =
  WebSocket.onMessage messageFromServer.address socket


main : Signal.Signal Html.Html
main =
  app.html


port tasks : Signal (Task.Task Effects.Never ())
port tasks =
  app.tasks
