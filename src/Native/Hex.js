Elm.Native.Hex = {};
Elm.Native.Hex.make = function(localRuntime) {

  localRuntime.Native = localRuntime.Native || {};
  localRuntime.Native.Hex = localRuntime.Native.Hex || {};

  if (localRuntime.Native.Hex.values){
    return localRuntime.Native.Hex.values;
  }
  
  function toHex(str) {
    return str.toString(16);
  }
  
  function fromHex(str) {
    console.log(parseInt(str, 16));
    return parseInt(str, 16);
  }
  
  localRuntime.Native.Hex.values = {
    toHex: toHex,
    fromHex: fromHex
  };
  return localRuntime.Native.Hex.values;
};