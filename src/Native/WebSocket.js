Elm.Native.WebSocket = {};
Elm.Native.WebSocket.make = function(localRuntime) {

  localRuntime.Native = localRuntime.Native || {};
  localRuntime.Native.WebSocket = localRuntime.Native.WebSocket || {};
  if (localRuntime.Native.WebSocket.values){
      return localRuntime.Native.WebSocket.values;
  }

  var Task = Elm.Native.Task.make(localRuntime);
  var Utils = Elm.Native.Utils.make(localRuntime);

  var connectedHandlers = {}; // url -> {address, socket}
  var onMessageHandlers = {}; // url -> {address, socket}

  function _register(handler, address, socket) {
    handler[socket.url] = {
      address: address,
      socket: socket
    };
  }

  function _deleteByValue(object, value) {
    for (var key in object) {
      if (object.hasOwnProperty(key)) {
        if (object[key] === value) {
          delete object[key];
        }
      }
    }
  }

  function connect(host) {
    var socket;
    return Task.asyncFunction(function(callback) {

      var sentSuccess = false;
      var path = 'ws://' + host;
      socket = new WebSocket(path);

      var sendConnectedToAddresses = function(val) {
        for (var url in connectedHandlers) { // key
          if (connectedHandlers.hasOwnProperty(url)) {
            Task.perform(connectedHandlers[url].address._0(val));
          }
        }
      };

      var removeFromHandlers = function(socket) {
        _deleteByValue(connectedHandlers, socket);
        _deleteByValue(onMessageHandlers, socket);
      };

      socket.onopen = function() {
        if (!sentSuccess) {
          sentSuccess = true;
          callback(Task.succeed(socket));
        }
        sendConnectedToAddresses(true);
      };

      socket.onmessage = function(event) {
        var message = event.data;
        for (var url in onMessageHandlers) { // key
          if (onMessageHandlers.hasOwnProperty(url)) {
            Task.perform(onMessageHandlers[url].address._0(message));
          }
        }
      };

      socket.onclose = function() {
        sendConnectedToAddresses(false);
        removeFromHandlers(socket);
      };

      socket.onerror = function() {
        callback(Task.fail('Error while opening a websocket to ' + path));
        // TODO see what the error is and say something more useful
      };
    });
  }

  function send(message, socket) {
    var error = function(errorMsg) {
      return 'Error while sending a message: ' + errorMsg;
    }
    return Task.asyncFunction(function(callback) {
      switch (socket.readyState) {
        case 0:
          callback(Task.fail(error('The connection is not yet open.')));
          break;
        case 1: // the happy case
          socket.send(message);
          callback(Task.succeed(Utils.Tuple0));
          break;
        case 2:
          callback(Task.fail(error('The connection is in the process of closing.')));
          break;
        case 3:
          callback(Task.fail(error('The connection is closed or couldn\'t be opened.')));
          break;
      }
    });
  }

  function onMessage(address, socket) {
    return Task.asyncFunction(function(callback) {

      _register(onMessageHandlers, address, socket);

      callback(Task.succeed(Utils.Tuple0));

    })
  }

  function connected(address, socket) {
    return Task.asyncFunction(function(callback) {

      var isConnected = socket.readyState === 1;
      Task.perform(address._0(isConnected));

      _register(connectedHandlers, address, socket);

      callback(Task.succeed(Utils.Tuple0));

    })
  }

  localRuntime.Native.WebSocket.values = {
    connect: connect,
    send: F2(send),
    onMessage: F2(onMessage),
    connected: F2(connected)
  };
  return localRuntime.Native.WebSocket.values;
};