module Protocol (..) where

import Json.Encode
import Json.Decode exposing ((:=))
import Task exposing (Task)
import WebSocket
import Types exposing (User, Message, Registration, MessagesAndRegistrations)


baseUrl : String
baseUrl = "//elm-talk-backend.herokuapp.com/"

registrationUrl : String
registrationUrl = baseUrl ++ "register"

websocketUrl : String
websocketUrl = baseUrl ++ "websocket"

socket : Task String WebSocket.WebSocket
socket =
  WebSocket.connect websocketUrl

-- todo: Task 1a)
-- http://package.elm-lang.org/packages/elm-lang/core/3.0.0/Json-Decode
userDecoder : Json.Decode.Decoder User
userDecoder =
  Json.Decode.succeed (User "foo" "bar")


messageDecoder : Json.Decode.Decoder Message
messageDecoder =
  Json.Decode.object2
    Message
    ("message" := Json.Decode.string)
    ("user" := Json.Decode.string)


messagesDecoder : Json.Decode.Decoder (List Message)
messagesDecoder =
  Json.Decode.list messageDecoder


registrationDecoder : Json.Decode.Decoder Registration
registrationDecoder =
  Json.Decode.object1
    Registration
    ("name" := Json.Decode.string)


registrationsDecoder : Json.Decode.Decoder (List Registration)
registrationsDecoder =
  Json.Decode.list registrationDecoder

messagesAndRegistrationDecoder : Json.Decode.Decoder MessagesAndRegistrations
messagesAndRegistrationDecoder =
  Json.Decode.object2
    MessagesAndRegistrations
    ("messages" := messagesDecoder)
    ("registrations" := registrationsDecoder)


-- todo: Task 1a)
-- http://package.elm-lang.org/packages/elm-lang/core/3.0.0/Json-Encode
registrationEncoder : Registration -> Json.Encode.Value
registrationEncoder registration =
  Json.Encode.null

messageEncoder : Message -> Json.Encode.Value
messageEncoder message =
  Json.Encode.object
    [ ( "message", Json.Encode.string message.message )
    , ( "user", Json.Encode.string message.user )
    ]


messagesEncoder : List Message -> Json.Encode.Value
messagesEncoder =
  Json.Encode.list << List.map messageEncoder
