module Types (..) where

import WebSocket


type Action
  = NoOp
  | Connect (Maybe WebSocket.WebSocket)
  | SendMessage String
  | SentMessage String
  | ReceiveMessages MessagesAndRegistrations
  | UpdateMessage String
  | JsonError String
  | Register String
  | UpdateUser String
  | SetUser User


type alias Model =
  { webSocket : Maybe WebSocket.WebSocket
  , messages : List Message
  , currentMessage : String
  , currentUser : Maybe User
  , currentUserRegistration : String
  , jsonError : String
  , registrations : List Registration
  }


type alias Message =
  { message : String
  , user : String
  }


type alias Registration =
  { name : String
  }


type alias User =
  { name : String
  , uuid : String
  }


type alias MessagesAndRegistrations =
  { messages : List Message
  , registrations : List Registration
  }

