module View (..) where

import Html
import Html.Events as E
import Html.Attributes as A
import Http
import Types exposing (Action(..), Model, Message, User, Registration)
import Signal
import Hex
import String
import Char


getHexFromUsername : String -> String
getHexFromUsername username =
  let
    hex =
      username
        |> String.toList
        |> List.map Char.toCode
        |> List.sum
        |> Hex.toHex

    color =
      if String.length hex < 6 then
        String.repeat 6 hex
      else
        hex
  in
    "#" ++ String.slice 0 6 color


getTextColor : String -> String
getTextColor hex =
  let
    red =
      toFloat <| Hex.fromHex <| String.slice 1 3 hex

    green =
      toFloat <| Hex.fromHex <| String.slice 3 5 hex

    blue =
      toFloat <| Hex.fromHex <| String.slice 5 7 hex

    threshold =
      red * 0.299 + green * 0.587 + blue * 0.114
  in
    if threshold > 186.0 then
      "#000000"
    else
      "#ffffff"


getUsername : Maybe User -> String
getUsername user =
  Maybe.map (\u -> u.name) user
    |> Maybe.withDefault ""


getFirstTwoLetters : String -> String
getFirstTwoLetters s =
  if String.length s > 1 then
    String.toUpper <| String.slice 0 2 s
  else
    s


{-| Task 2b) display a single message -}
viewMessage : Message -> Html.Html
viewMessage m =
    Html.li
    []
    []


{-| Task 2a) display a list of messages
-}
viewMessages : List Message -> Html.Html
viewMessages ms =
  Html.ul
    []
    []


{-| Task 2d) display a registration
-}
viewRegistration : Registration -> Html.Html
viewRegistration r =
  Html.li
    []
    []


{-| Task 2c) display a list of registrations
-}
viewRegistrations : List Registration -> Html.Html
viewRegistrations rs =
  Html.div
    []
    []


chatView : Signal.Address Action -> Model -> User -> Html.Html
chatView address model user =
  Html.div
    [ A.class "container row"
    ]
    [ Html.node
        "main"
        []
        [ Html.div
            []
            [ Html.h4
                []
                [ Html.text "Chat" ]
            ]
        , Html.div
            [ A.class "col s9" ]
            [ viewMessages model.messages
            ]
        , Html.div
            [ A.class "col s3" ]
            [ viewRegistrations model.registrations
            ]
        ]
    , Html.div
        [ A.class "page-footer"
        , A.style
            [ ( "position", "fixed" )
            , ( "bottom", "10px" )
            , ( "left", "0" )
            , ( "right", "0" )
            ]
        ]
        [ Html.div
            [ A.class "container"
            ]
            [ Html.input
                [ A.placeholder "Text to send"
                , A.value model.currentMessage
                , E.on "input" E.targetValue (\x -> Signal.message address (UpdateMessage x))
                , E.onKeyDown
                    address
                    (\k ->
                      if k == 13 then
                        (SendMessage model.currentMessage)
                      else
                        NoOp
                    )
                ]
                []
            , Html.div [ A.class "btn waves-effect waves-light", E.onClick address (SendMessage model.currentMessage) ] [ Html.text "send" ]
            ]
        ]
    ]


registerView : Signal.Address Action -> Model -> Html.Html
registerView address model =
  Html.node
    "main"
    [ A.class "container row login-container" ]
    [ Html.h4 [] [ Html.text "Registration" ]
    , Html.input
        [ A.placeholder "Username"
        , A.value model.currentUserRegistration
        , E.on "input" E.targetValue (\x -> Signal.message address (UpdateUser x))
        ]
        []
    , Html.div
        [ A.class "btn waves-effect waves-light", E.onClick address (Register model.currentUserRegistration) ]
        [ Html.text "send" ]
    ]


view : Signal.Address Action -> Model -> Html.Html
view address model =
  Html.div
    []
    [ Html.node "link" [ A.rel "stylesheet", A.type' "text/css", A.href "libs/materialize.min.css" ] []
    , Html.header
        []
        [ Html.nav
            []
            [ Html.div
                [ A.class "nav-wrapper" ]
                [ Html.a
                    [ A.class "brand-logo", A.href "#" ]
                    [ Html.text "  >> F# - Elm Chat" ]
                , Html.ul
                    [ A.class "right hide-on-med-and-down", A.id "nav-mobile" ]
                    [ Html.li
                        []
                        [ Html.a
                            [ A.href "#" ]
                            [ Html.text <| "Logged in as: " ++ getUsername model.currentUser ]
                        ]
                    ]
                ]
            ]
        ]
    , case model.currentUser of
        Nothing ->
          registerView address model

        Just user ->
          chatView address model user
    ]
