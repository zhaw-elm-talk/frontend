module WebSocket (connect, send, onMessage, connected, WebSocket) where

{-|
A module for working with
[HTML5 WebSockets](https://developer.mozilla.org/cs/docs/Web/API/WebSocket)
through the Elm Task API.

# Creating a WebSocket
@docs connect

# Sending and Receiving
These functions should be used with `Task.andThen` to provide them with a socket
obtained with `connect`.
@docs send, onMessage, connected

# The Socket Type
@docs WebSocket
-}

import Native.WebSocket
import Task exposing (Task)


{-| An opaque type representing a WebSocket. You can't access anything directly,
only through the API provided above.
-}
type WebSocket
  = WebSocket


{-| Create a socket, given a hostname and a port number.

    socket = connect "localhost" 8001

The task can fail with an appropriate message if the server is unreachable
or the client doesn't support WebSockets.
-}
connect : String -> Task String WebSocket
connect =
  Native.WebSocket.connect


{-| Send a string on the socket. To serialize your Elm values, use `toString`
or `Json.Encode`.

    port outgoing = socket `Task.andThen` send "Testing 1 2 3"

The task can fail with an appropriate message if the sending fails.
-}
send : String -> WebSocket -> Task String ()
send =
  Native.WebSocket.send


{-| Receive data at a mailbox as a string. If data received is not already
a string, it will be JSON-encoded. Unserializable JS values become `"null"`,
this is a good initial value when you set up the mailbox.

    mailbox = Signal.mailbox "null"
    port incoming = socket `Task.andThen` onMessage mailbox.address
-}
onMessage : Signal.Address String -> WebSocket -> Task x ()
onMessage =
  Native.WebSocket.onMessage


{-| Set up a signal of bools indicating whether or not the socket is connected.
You should initialize the mailbox to `False`; if the server is available a
`True` event will be sent almost immediately. If the server is not available,
`connect` will fail. If the socket disconnects (and then reconnects later),
the address's mailbox's signal will have events indicating that.
-}
connected : Signal.Address Bool -> WebSocket -> Task x ()
connected =
  Native.WebSocket.connected